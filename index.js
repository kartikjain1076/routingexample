/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Drawer from './Drawer';

// AppRegistry.registerComponent(appName, () => Drawer);
AppRegistry.registerComponent(appName, () => App);