import React,{Component} from 'react';
import {Text,View,Button} from 'react-native';

export default class Home extends Component {
    render(){
        return(
            <View>
                <Text>Home Page</Text>
                <Button title = 'Go to Detail' onPress = {() => this.props.navigation.navigate('Details')} />
                <Button title = 'Go to Notiication' onPress = {() => this.props.navigation.navigate('Notification')} />
                <Button title = 'Go Back' onPress = {() => this.props.navigation.goBack()} />
                <Button title = 'Open Drawer' onPress = {() => this.props.navigation.openDrawer()} />
            </View>
        )
    }
}