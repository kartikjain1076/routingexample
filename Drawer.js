import {createDrawerNavigator, createAppContainer} from 'react-navigation';
import Home from './Home';
import App from './App';

const drawerNavigator = createDrawerNavigator({
 App : App
},
{
  initialRouteName : 'App',
//   drawerWidth : 200,
//   drawerPosition : 'right',
//   contentComponent : Home
})

const Drawer = createAppContainer(drawerNavigator);

export default Drawer;