REACT NATIVE ROUTING

Install ‘react-navigation’ module with the help of ‘npm install react-navigation’ command.
Now install ‘react-native-gesture-handler’ module with the help of ‘npm install react-native-gesture-handler’ command.
react-native-gesture -handler module is needed because in some navigator like in drawer we need gesture control like to open the drawer.
In some cases we also need to run ‘npm install’ command. It has to run when the command in the next line gives you error.
Run ‘react-native link’ to link all the modules to the android project.
Now open mainActivity.java which can be found in android -> app -> src -> main -> java -> com -> (yourProjectName) -> mainActivity.java
And the following lines of code
+ import com.facebook.react.ReactActivityDelegate;
+ import com.facebook.react.ReactRootView;
+ import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;
+  @Override
+  protected ReactActivityDelegate createReactActivityDelegate() {
+    return new ReactActivityDelegate(this, getMainComponentName()) {
+      @Override
+      protected ReactRootView createRootView() {
+       return new RNGestureHandlerEnabledRootView(MainActivity.this);
+      }
+    };
+  }
Now rebuild the project by ‘react-native run-android’.
Now we all set to build our first navigator.
To make the stack navigator:
import {createStackNavigator, createAppContainer} from 'react-navigation';
import Home from './Home';
import Details from './Details';
import Notification from './Notification';

const stackNavigator = createStackNavigator({
 Home : Home,
 Details : Details,
 Notification : Notification
},
{
 initialRouteName : 'Home',
 headerMode : 'none'
})

const App = createAppContainer(stackNavigator);

export default App;
To make Drawer Navigator:
import {createDrawerNavigator, createAppContainer} from 'react-navigation';
import Home from './Home';
import Details from './Details';
import Notification from './Notification';

const drawerNavigator = createDrawerNavigator({
 Home : Home,
 Details : Details,
 Notification : Notification


},
{
 initialRouteName : 'App',
 drawerWidth : 200,
 drawerPosition : 'right',
 contentComponent : Home
})

const Drawer = createAppContainer(drawerNavigator);

export default Drawer;
To make Tab Navigator:
import {createBottomTabNavigator, createAppContainer} from 'react-navigation';

import Home from './Home';
import Details from './Details';
import Notification from './Notification';

const tabNavigator = createBottomTabNavigator({
Homee : Home,
Detailss : Details,
Notificationss : Notification
})

const Tab = createAppContainer(tabNavigator);

export default Tab;
To navigate to another screen:
<Button title = 'Go to Home' onPress = {() => this.props.navigation.navigate('Home')} />
To Go Back:
<Button title = 'Go to Home' onPress = {() => this.props.navigation.goBack()} />
To push a screen in stack that already exists:
<Button title = 'Go to Home' onPress = {() => this.props.navigation.push('Home')} />
