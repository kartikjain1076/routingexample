import React,{Component} from 'react';
import {Text,View,Button} from 'react-native';

export default class Details extends Component {
    render(){
        return(
            <View>
                <Text>Details Page</Text>
                <Button title = 'Go to Home' onPress = {() => this.props.navigation.navigate('Home')} />
                <Button title = 'Go to Notiication' onPress = {() => this.props.navigation.navigate('Notification')} />
                <Button title = 'Go Back' onPress = {() => this.props.navigation.goBack()} />
                <Button title = 'Open Drawer' onPress = {() => this.props.navigation.openDrawer()} />
            </View>
        )
    }
}