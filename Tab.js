import {createBottomTabNavigator, createAppContainer} from 'react-navigation';

import Home from './Home';
import Details from './Details';
import Notification from './Notification';

const tabNavigator = createBottomTabNavigator({
 Homee : Home,
 Detailss : Details,
 Notificationss : Notification
})

const Tab = createAppContainer(tabNavigator);

export default Tab;