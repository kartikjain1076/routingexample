import React,{Component} from 'react';
import {Text,View,Button} from 'react-native';

export default class Notification extends Component {
    render(){
        return(
            <View>
                <Text>Notification Page</Text>
                <Button title = 'Go to Home' onPress = {() => this.props.navigation.navigate('Home')} />
                <Button title = 'Go to Detail' onPress = {() => this.props.navigation.navigate('Details')} />
                <Button title = 'Go Back' onPress = {() => this.props.navigation.goBack()} />
                <Button title = 'Open Drawer' onPress = {() => this.props.navigation.openDrawer()} />
                <Button title = 'Go to Home2' onPress = {() => this.props.navigation.push('Home')} />
            </View>
        )
    }
}