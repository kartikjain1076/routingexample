import {createStackNavigator, createAppContainer} from 'react-navigation';
import Home from './Home';
import Details from './Details';
import Notification from './Notification';
import Tab from './Tab';

const stackNavigator = createStackNavigator({
  // Tab : Tab,
  Home : Home,
  Details : Details,
  Notification : Notification
},
{
  initialRouteName : 'Home',
  // headerMode : 'none'
})

const App = createAppContainer(stackNavigator);

export default App;